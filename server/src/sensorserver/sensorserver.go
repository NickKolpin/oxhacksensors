package main

import (
	"fmt"
	"net/http"
    "net/url"
    "encoding/json"
    "strconv"
    "strings"
    "time"
)

type device interface {
    update(string, url.Values)
    get(string) (string, error)
}

type door struct{
    Open bool
    Timestamp time.Time
}

type doors struct {
    doors map[string]*door
}

func (ds doors) update(id string, vals url.Values) {
    d, ok := d.doors[id]
    if !ok {
        return
    }

}

func (ds doors) get(id string) (string, err) {
    d, ok := ds.doors[id]
    if ok {
        return json.Marshal(d)
    } else {
        return json.Marshal(ds.doors)
    }
}

type pot struct {
    Voltage float32
    Timestamp time.Time
}

var pots map[string]*pot

func (d door) String() string {
    s := ""
    if d.Open {
        s += "Open"
    } else {
        s += "Closed"
    }
    return s
}

var doors map[string]*door

func handledoor(w http.ResponseWriter, r *http.Request) {
    r.ParseForm()
    if r.Method == "GET" {
        fmt.Printf("form = %v\n", r.Form)
        id := strings.Replace(r.URL.Path, "/door/", "", 1)
        fmt.Printf("GET request on id = %s\n", id)
        d, ok := doors[id]
        if ok {
            js, err := json.Marshal(d)
            if err != nil {
                http.Error(w, err.Error(), http.StatusInternalServerError)
                return
            }
            w.Header().Set("Content-Type", "application/json")
            w.Write(js)
        } else {
            js, err := json.Marshal(doors)
            if err != nil {
                http.Error(w, err.Error(), http.StatusInternalServerError)
                return
            }
            w.Header().Set("Content-Type", "application/json")
            w.Write(js)

        }
    } else if r.Method == "POST" {
    //fmt.Printf("Received:\n%v\n", r)
        id := strings.Replace(r.URL.Path, "/door/", "", 1)
        fmt.Printf("POST request on id = %s\n", id)
        if d, ok := doors[id]; ok {
            fmt.Printf("form = %v\n", r.PostForm)
            if val, ok := r.PostForm["button"]; ok {
                state := val[0] == "1"
                fmt.Printf("val = %v, state = %v\n", val, state)
                d.Open = state
                t := time.Now()
                if ts, ok := r.PostForm["time"]; ok {
                    if tunix, err := strconv.ParseInt(ts[0], 10, 64); err == nil {
                        t = time.Unix(tunix, 0)
                    }
                }
                d.Timestamp = t
                fmt.Printf("doors = %v\n", doors)
            }
        }
    }
}

func handlepot(w http.ResponseWriter, r *http.Request) {
    r.ParseForm()
    if r.Method == "GET" {
        fmt.Printf("form = %v\n", r.Form)
        id := strings.Replace(r.URL.Path, "/pot/", "", 1)
        fmt.Printf("GET request on id = %s\n", id)
        p, ok := pots[id]
        if ok {
            js, err := json.Marshal(p)
            if err != nil {
                http.Error(w, err.Error(), http.StatusInternalServerError)
                return
            }
            w.Header().Set("Content-Type", "application/json")
            w.Write(js)
        } else {
            js, err := json.Marshal(pots)
            if err != nil {
                http.Error(w, err.Error(), http.StatusInternalServerError)
                return
            }
            w.Header().Set("Content-Type", "application/json")
            w.Write(js)

        }
    } else if r.Method == "POST" {
    //fmt.Printf("Received:\n%v\n", r)
        id := strings.Replace(r.URL.Path, "/pot/", "", 1)
        fmt.Printf("POST request on id = %s\n", id)
        if p, ok := pots[id]; ok {
            fmt.Printf("form = %v\n", r.PostForm)
            if val, ok := r.PostForm["pot"]; ok {
                state, err := strconv.ParseFloat(val[0], 32)
                if err == nil {
                    fmt.Printf("val = %v, state = %v\n", val, float32(state))
                    p.Voltage = float32(state)
                    t := time.Now()
                    if ts, ok := r.PostForm["time"]; ok {
                        if tunix, err := strconv.ParseInt(ts[0], 10, 64); err == nil {
                            t = time.Unix(tunix, 0)
                        }
                    }
                    p.Timestamp = t
                    fmt.Printf("pots = %v\n", pots)
                }
            }
        }
    }
}
func main() {
    doors = make(map[string]*door)
    doors["0"] = &door{}
    doors["1"] = &door{}
    pots = make(map[string]*pot)
    pots["0"] = &pot{}
	fmt.Printf("Starting server...\n")
	http.HandleFunc("/door/", handledoor)
	http.HandleFunc("/pot/", handlepot)
	http.ListenAndServe(":8080", nil)
}
