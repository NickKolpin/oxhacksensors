Oxford Hackspace sensor network
===============================

*A bunch of sensors measuring things at the space and an easy way for programs to access the data.*

Got a type of sensor you'd like to see measuring stuff in the hackspace? Add an entry on [Sensor Ideas](https://bitbucket.org/NickKolpin/oxhacksensors/wiki/Sensor%20Ideas).

Want to learn the API to either add a new sensor to the network or write a client program to use the network's data? Check out the [API](https://bitbucket.org/NickKolpin/oxhacksensors/wiki/API) page. 
